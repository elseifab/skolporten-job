# Skolporten Jobb
Plugin för att lista jobb på Skolportens nya webb.

## Installation
1. Installera som vanlig plugin.
2. Aktivera pluggen!
3. Gå till pluggens inställningar: wp-admin > Settings > Skolporten Job
4. Välj fliken API Connection
5. Ange URL = http://newsflow.skolporten.se/api samt Domain = skp

## Jobblistning
För att visa lista på sida, ange shortcode [skolporten-job]

Ställ om sökväg till mallar i inställningarna och välj mall i shortcode.
För att byta mallnamn, sätt parametern "template" i shortcode, exempel: [skolporten-job template="chefer"]

### Koder för utvisning

Posts innehåller jobbannonser.
{{length}} är antal poster i en listning.

#### #posts
* {{date_application}}, sista ansökningsdag
* {{post_title}}, rubrikrad
* {{url}}, webbadress till slutdestination
* {{click}}, antal klick på puffen
* {{contract_url}}, webbadress till arbetsgivarpresentation
* {{contract_title}}, arbetsgivarens namn
* {{post_content}}, innehållet i puffen, *OBS!* Tre måsvingar för html-utvisning: {{{post_content}}}

## Arbetsgivare
För att visa arbetsgivarpresentation:

1. Skapa sida med shortcode [skolporten-contract]

2. Lägg in url till sidan i inställningar för plugin; wp-admin > Settings > Skolporten Job, general

