<?php

namespace skolporten\job\structure;

class Scripts {

	function __construct() {
//		add_action( 'wp_enqueue_scripts', array( &$this, 'wp_enqueue_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( &$this, 'admin_enqueue_scripts' ) );
	}

	function wp_enqueue_scripts() {
	}

	function admin_enqueue_scripts() {

		if( isset( $_REQUEST['page'] ) && strpos( $_REQUEST['page'], 'skpjob-' ) === 0 ) {

			wp_enqueue_script( 'knockout-js', get_skolporten_job_plugin_url() . '/assets/js/knockout.min.js', array( 'jquery' ), false, true );

			wp_enqueue_style( 'bootstrap-css', get_skolporten_job_plugin_url() . '/assets/bootstrap/css/bootstrap.min.css' );
			wp_enqueue_script( 'bootstrap-js', get_skolporten_job_plugin_url() . '/assets/bootstrap/js/bootstrap.min.js', array( 'jquery' ), false, true );



		}

	}

}

new Scripts();