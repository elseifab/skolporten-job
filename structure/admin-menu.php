<?php

namespace skolporten\job\structure;

class AdminMenu {
	function __construct() {
		add_action( 'admin_menu', array( &$this, 'menues' ) );
	}

	function menues() {

		$handle = add_options_page( 'Skolporten Job', 'Skolporten Job', 'manage_options', 'skpjob-setting', array( 'skolporten\job\admin\Settings', 'display' ) );

	}

}

new AdminMenu();