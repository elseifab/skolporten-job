<?php

namespace skolporten\job\app;
use skolporten\job\helpers\API;
use skolporten\job\settings\General;

class Contracts {

	function __construct() {
		add_shortcode( 'skolporten-contract', array( &$this, 'display' ) );
	}

	function display( $atts ) {

		ob_start();

		?>
		Contract/employer presentation
		<?php

		$result = ob_get_contents();
		ob_end_clean();

		return $result;
	}

}

new Contracts();