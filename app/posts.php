<?php

namespace skolporten\job\app;

use skolporten\job\helpers\API;
use skolporten\job\settings\General;

class Posts
{

    function __construct()
    {
        add_shortcode( 'skolporten-job', array( &$this, 'display' ) );
    }

    function display( $atts )
    {

        $settings = get_option( 'skolporten-job-settings', array() );

        $template_dir = General::template_dir( $settings );
        $template     = isset( $atts['template'] ) ? $atts['template'] : 'default';

        $objects = API::post( '/v1/posts/search',
            array(
                'date_application' => time()
            )
        );

        $data = array(
            'posts' => array()
        );

        if (sizeof( $objects )) {
            foreach ($objects as $object) {

                if (isset( $object->date_application )) {
                    $object->date_application = date_i18n( General::date_format( $settings ),
                        $object->date_application );
                }
                $item = get_object_vars( $object );

                $item['url'] = $settings['url'] . '/v1/click/' . $settings['id'] . '/' . $object->ID;
                $contract_id = isset( $object->contract_id ) ? $object->contract_id : '';

                if (!isset( $item['contract_url'] )) {
                    $item['contract_url'] = General::contract_url( $settings ) . '/?contract_id=' . $contract_id;
                }
                $item['contract_title'] = isset( $object->contract_title ) ? $object->contract_title : 'no contract name';

                $data['posts'][] = $item;
                $data['length']  = sizeof( $item );

            }
        }

        ob_start();

        ?>
        <!--[SEARCH PARAMETERS HERE]-->

        <?php

        $m = new \Mustache_Engine( array(
            'loader' => new \Mustache_Loader_FilesystemLoader( $template_dir ),
        ) );

        echo $m->render( $template, $data );

        $result = ob_get_contents();
        ob_end_clean();

        //wp_enqueue_style( 'skolporten-job-css', get_skolporten_job_plugin_url() . '/app/style.css' );

        return $result;
    }

}

new Posts();