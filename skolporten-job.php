<?php
/*
Plugin Name: Skolporten Job
Plugin URI: http://wordpress.org/plugins/skolporten-job/
Description: Listing posts from Newsflow as a shortcode plugin for Skolporten listing jobs
Author: Flowcom
Version: 2.2
Author URI: http://www.flowcom.se/
*/

include_once 'lib/Mustache/Autoloader.php';
\Mustache_Autoloader::register();

include_once 'helpers/newsflow-api.php';

include_once 'structure/admin-menu.php';
include_once 'structure/scripts.php';
include_once 'structure/tabs.php';

include_once 'admin/settings.php';

include_once 'app/posts.php';
include_once 'app/contracts.php';

function get_skolporten_job_plugin_url() {
	return plugins_url( '' , __FILE__ );
}