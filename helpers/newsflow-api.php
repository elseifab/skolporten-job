<?php
namespace skolporten\job\helpers;

class API {

	static function settings() {
		return get_option( 'skolporten-job-settings', array() );
	}

	static function get( $api_url, $data ) {
		$settings = API::settings();
		$data     = API::prepare_data( $data );
		$args = array(
				'body' => json_encode( $data ),
		);
		$request  = wp_remote_get( $settings['url'] . $api_url, $args );
		$response = wp_remote_retrieve_body( $request );
		return json_decode( $response );
	}

	static function post( $api_url, $data ) {
		$settings = API::settings();
		$data     = API::prepare_data( $data );

		$args = array(
				'body' => json_encode( $data ),
		);
		$request = wp_remote_post( $settings['url'] . $api_url, $args	);
		$response = wp_remote_retrieve_body( $request );
		return json_decode( $response );
	}

	static function prepare_data( $data1 = array(), $data2 = array(), $data3 = array() ) {

		$settings = API::settings();
		$data     = array();

		if ( is_object( $data1 ) ) $data1 = get_object_vars( $data1 );
		if ( is_object( $data2 ) ) $data2 = get_object_vars( $data2 );
		if ( is_object( $data3 ) ) $data3 = get_object_vars( $data3 );

		if ( sizeof( $data1 ) ) $data = array_merge( $data, $data1 );
		if ( sizeof( $data2 ) ) $data = array_merge( $data, $data2 );
		if ( sizeof( $data3 ) ) $data = array_merge( $data, $data3 );

		$data['domain'] = $settings['id'];
		//$data['token']  = $settings['token'];
		// remove all zero array values
		foreach ( $data as $key => $d ) {
			if ( is_array( $d ) && sizeof( $d ) === 1 ) {
				$data[$key] = $d[0];
			}
		}

		return $data;

	}

}