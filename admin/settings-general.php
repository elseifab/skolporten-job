<?php

namespace skolporten\job\settings;

class General {

	public $settings;

	function __construct() {
		$this->settings = get_option( 'skolporten-job-settings', array() );
		add_filter( 'skolporten_job_structure_Tabs_item', array( &$this, 'item' ), 10 );
		add_action( 'skolporten_job_structure_Tabs_display_general', array( &$this, 'display' ) );
		add_action( 'skolporten_job_structure_Tabs_update_general', array( &$this, 'update' ) );
	}

	function item( $items ) {
		$items[ 'general' ] = 'General';
		return $items;
	}

	function display() {

		$template_dir = General::template_dir( $this->settings );
		$contract_url = General::contract_url( $this->settings );
		$date_format = General::date_format( $this->settings );

		?>

		<div class="form-group">
			<label for="template_dir">Template Directory</label>
			<input class="form-control" name="template_dir" value="<?php echo $template_dir; ?>" type="text" id="template_dir" style="max-width: 600px;" />
			<p class="help-block">Absolute directory path to Mustache template directory, no trailing slash</p>
		</div>

		<div class="form-group">
			<label for="date_format">Date format</label>
			<input class="form-control" name="date_format" value="<?php echo $date_format; ?>" type="text" id="date_format" style="max-width: 100px;" />
			<p class="help-block">Date format for output in listnings, eg YYYY-mm-dd</p>
		</div>

		<div class="form-group">
			<label for="contract_url">Contract path</label>
			<input class="form-control" name="contract_url" value="<?php echo $contract_url; ?>" type="text" id="contract_url" style="max-width: 600px;" />
			<p class="help-block">Url to page with contract/employer presentation, no trailing slash</p>
		</div>


	<?php

	}

	function update() {
		$this->settings = get_option( 'skolporten-job-settings', array() );
		$this->settings['template_dir'] = esc_attr( $_REQUEST['template_dir'] );
		$this->settings['contract_url'] = esc_attr( $_REQUEST['contract_url'] );
		$this->settings['date_format'] = esc_attr( $_REQUEST['date_format'] );
		update_option( 'skolporten-job-settings', $this->settings );

		?>
		<div class="alert alert-success">
			General settings saved.
		</div>
		<?php
	}

	static function template_dir( $settings ) {
		$template_dir = isset( $settings['template_dir'] ) && !empty( $settings['template_dir'] ) ? $settings['template_dir'] : WP_PLUGIN_DIR . '/skolporten-job/templates';
		return $template_dir;
	}

	static function contract_url( $settings ) {
		$url = isset( $settings['contract_url'] ) && !empty( $settings['contract_url'] ) ? $settings['contract_url'] : '/arbetsgivare';
		return $url;
	}

	static function date_format( $settings ) {
		$format = isset( $settings['date_format'] ) && !empty( $settings['date_format'] ) ? $settings['date_format'] : 'Y-m-d';
		return $format;
	}

}

new General();