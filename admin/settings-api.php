<?php

namespace skolporten\job\settings;

class Api {

	public $settings;

	function __construct() {
		$this->settings = get_option( 'skolporten-job-settings', array() );
		add_filter( 'skolporten_job_structure_Tabs_item', array( &$this, 'item' ), 30 );
		add_action( 'skolporten_job_structure_Tabs_display_api', array( &$this, 'display' ) );
		add_action( 'skolporten_job_structure_Tabs_update_api', array( &$this, 'update' ) );
	}

	function item( $items ) {
		$items['api'] = 'API Connection';
		return $items;
	}

	function display() {

		$url   = $this->settings['url'];
		//$token   = $this->settings['token'];
		$domain   = $this->settings['id'];

		if( empty( $domain ) ) {
			$domain = str_replace( '.', '-', $_SERVER['HTTP_HOST'] );
		}

		$valid_url   = false;
		$valid_token = false;

		if ( ! empty( $url ) && get_headers( $url ) ) {
			$response = \skolporten\job\helpers\API::get( '/v1/ping', array() );
			if ( $response->success ) $valid_url = true;
		}

		$feedback_url = "";
		$icon_url     = "";
		if ( $valid_url ) {
			$feedback_url = "has-success";
			$icon_url     = "glyphicon-ok";
		}
		else {
			$feedback_url = "has-error";
			$icon_url     = "glyphicon-remove";
		}

		?>

		<p>
			Newsflow needs an API to search data from Newsflow.
		</p>

		<div class="form-group has-feedback <?php echo $feedback_url; ?>" style="width: 350px;">
			<label for="url">Url:</label>
			<input name="url" value="<?php echo $url; ?>" type="text" class="form-control input-md" id="url" style="width: 250px;" placeholder="Newsflow URL token here" /><span class="glyphicon <?php echo $icon_url; ?> form-control-feedback"></span>
			<p class="help-block">Address to the server where the Newsflow API exists</p>
		</div>

		<div class="form-group" style="width: 350px;">
			<label for="domain">Domain:</label>
			<input name="domain" value="<?php echo $domain; ?>" type="text" class="form-control input-md" id="domain" style="width: 250px;" placeholder="Domain name" />
			<p class="help-block">Unique domain name to store data separate in the API containers, often host name</p>
		</div>

	<?php

	}

	function update() {

		$this->settings = get_option( 'skolporten-job-settings', array() );
		$this->settings['url'] = esc_attr( $_REQUEST['url'] );
		$this->settings['id'] = esc_attr( $_REQUEST['domain'] );
		update_option( 'skolporten-job-settings', $this->settings );

		?>
		<div class="alert alert-success">
			API registered.
		</div>
	<?php
	}

}

new Api();